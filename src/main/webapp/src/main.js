'use strict';

angular.module('restaurant')
    .controller('mainController', function ($scope,LoginLogoutSrv,$location,$rootScope) {

        $scope.logout = function() {
            LoginLogoutSrv.logout();
            $location.path('/');
            console.log("bye bye");
        };

        $scope.hasAnyPermission = function(authorities) {
            var hasPermission = false;

            $rootScope.authDetails.permissions.forEach(function(permission) {
                authorities.forEach(function(authority) {
                    if (permission.authority === authority) {
                        hasPermission = true;
                    }
                });
            });

            return hasPermission;
        };


    });
