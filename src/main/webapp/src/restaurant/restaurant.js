'use strict';

angular.module('restaurant')
  .controller('restaurantCtrl', function($scope, $rootScope,RestSrv, $http, SERVICE_PATH) {
	   $rootScope.statusMenu = true;
	   
	    $scope.restaurant = {};
	    $scope.restaurants = [];
	    $scope.searchShow = false;
	    
	    $scope.showAddEditRestaurant = false;
	    $scope.hideListRestaurant = false;
	   
	   var urlRestaurant  = SERVICE_PATH.PRIVATE_PATH + '/restaurant';

		RestSrv.find(urlRestaurant, function(data) {
			$scope.restaurants = data;
		});
		 
		$scope.searchRestaurant = function(name) {
			RestSrv.findByName(urlRestaurant + "/" + name, function(data) {
			  $scope.findNm = data;
			  $scope.searchShow = true;
			});
		}
		 
	    $scope.show = function() {
	      $scope.showAddEditRestaurant = true;
	      $scope.hideListRestaurant = true;
	      $scope.searchShow = false;
	      $scope.findNm=null;
	    };

	    $scope.hide = function() {
	      $scope.showAddEditRestaurant = false;
	      $scope.hideListRestaurant = false;
	      $scope.restaurant = {};
	    };

	    $scope.editRestaurant = function(restaurant) {
	      $scope.restaurant = angular.copy(restaurant);
	      $scope.show();
	    };

	      $scope.deleteRestaurant = function(restaurant) {
		   RestSrv.delete(urlRestaurant, restaurant, function() {
		   $scope.restaurants.splice($scope.restaurants.indexOf(restaurant), 1);
	  });
	};
	
	$scope.saveRestaurant = function(restaurant) {
	      if (restaurant.id) {
	        RestSrv.edit(urlRestaurant, restaurant, function() {
	          delete restaurant.password;

	          for (var i = 0; i < $scope.restaurants.length; i++) {
	            if ($scope.restaurants[i].id === restaurant.id) {
	              $scope.restaurants[i] = restaurant;
	            }
	          }

	          $scope.hide();
	        });
	      } else {
	        RestSrv.add(urlRestaurant, restaurant, function(newRestaurant) {
	          $scope.restaurants.push(newRestaurant);
	          $scope.hide();
	        });
	      }
	    };

  });