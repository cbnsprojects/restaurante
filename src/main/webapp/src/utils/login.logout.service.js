'use strict';

angular.module('restaurant')
  .service('LoginLogoutSrv', function($http, $cookies, $rootScope, $location, $localStorage, SERVICE_PATH, $mdToast) {
    var serviceFactory = {};
    var urlLogin  = SERVICE_PATH.PUBLIC_PATH + '/login';
    var urlLogout = SERVICE_PATH.PUBLIC_PATH + '/logout';

    serviceFactory.login = function(name, password) {
      var requestParams = {
        method: 'GET',
        url: urlLogin,
        headers: {
          'Content-Type': 'application/json',
          'authorization' : 'Basic ' + btoa(name + ':' + password)
        }
      };

      $http(requestParams).then(
        function success(response) {
          var data = response.data;

          if (data.name) {
            $rootScope.authDetails = { name: data.name, authenticated: data.authenticated, permissions: data.authorities };
            $localStorage.authDetails = $rootScope.authDetails;
            $location.path('/home');
            $mdToast.show($mdToast.simple().textContent('Bem Vindo: ' + data.name + '.').position('top right').hideDelay(3000));
            console.log($rootScope.authDetails);
          } else {
            $rootScope.authDetails = { name: '', authenticated: false, permissions: [] };
          }
        },
        function failure(response) {
          $rootScope.authDetails = { name: '', authenticated: false, permissions: []};
          $mdToast.show($mdToast.simple().textContent("E-mail ou senha que você digitou não correspondem aos nossos registros").position('top right').hideDelay(3000));
        }
      );
      
      if($rootScope.authDetails.name ==''){
          return false;
       }
      
      return true;
    };

    serviceFactory.logout = function() {
      var requestParams = {
        method: 'POST',
        url: urlLogout,
        headers: { 'Content-Type': 'application/json' }
      };

      $http(requestParams).finally(function success(response) {
        delete $localStorage.authDetails;
        $rootScope.authDetails = { name: '', authenticated: false, permissions: [] };
        $location.path("/");
      });
    };

    serviceFactory.verifyAuth = function() {
      if ($localStorage.authDetails) {
        $rootScope.authDetails = $localStorage.authDetails;
      }
    };

    return serviceFactory;
  });
