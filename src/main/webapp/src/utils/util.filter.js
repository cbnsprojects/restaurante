'use strict';

angular.module('restaurant')
  .filter('formatPermission', function() {
    return function(input) {
      switch (input) {
        case 'ROLE_ADMIN':
          return 'Administrador';
        break;

        case 'ROLE_USER':
          return 'Usuario';
        break;

        default:
          return 'Unknown';
        break;
      };
    };
  });
