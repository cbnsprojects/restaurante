'use strict';

angular.module('restaurant')
  .controller('userCtrl', function($scope,$rootScope, $http, RestSrv, SERVICE_PATH){
	$rootScope.statusMenu = true;
	
    $scope.user = {};
    $scope.users = [];
    $scope.permissions = [];
    $scope.showAddEditUser = false;
    
    var userUrl = SERVICE_PATH.PRIVATE_PATH + '/user';

	 RestSrv.find(userUrl, function(data) {
		  $scope.users = data;
	  });
    
    $scope.show = function(){
      $scope.showAddEditUser = true;
    };

    $scope.hide = function(){
      $scope.showAddEditUser = false;
      $scope.user = {};
    };

    $scope.editUser = function(user){
      $scope.user = angular.copy(user);
      $scope.show();
    };

    $scope.deleteUser = function(user) {
    	RestSrv.delete(userUrl, user, function() {
	      $scope.users.splice($scope.users.indexOf(user), 1);
       });
	 };

    
  });
