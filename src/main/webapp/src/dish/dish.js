'use strict';

angular.module('restaurant')
  .controller('dishCtrl', function($scope, RestSrv, $route, $timeout,$location, $rootScope, $http, SERVICE_PATH) {
	   $rootScope.statusMenu = true;
	   
	    $scope.plates = [];
	    $scope.showAddEditDish = false;
	    $scope.restaurants = [];
	    $scope.searchShow = false;
	    
	    $scope.selectedDevice = {};

	    $scope.dish =  {
	            name: "",
	            price: "",
	            restaurant: {
	            	id: "",
	            	name: "",
	            	telephone: "",
	            	address: ""
	            }
	        }
	    
	   
	   var urlRestaurant  = SERVICE_PATH.PRIVATE_PATH + '/restaurant';
	   
	   var urlDish  = SERVICE_PATH.PRIVATE_PATH + '/dish';
	    
   	    $scope.showSelectValue = function(item) {
		 RestSrv.findById(urlRestaurant + '/id/' + item, function(data) {
			  $scope.dish.restaurant = data;
		  });
		};
	    
		 RestSrv.find(urlDish, function(data) {
			  $scope.plates = data;
		  });
		 
		 RestSrv.find(urlRestaurant, function(data) {
			 $scope.restaurants=[];
			  $scope.restaurants = data;
		  });
		 
		 $scope.searchDish = function(name) {
			RestSrv.findByName(urlDish + "/" + name, function(data) {
			  $scope.findNm = data;
			  $scope.searchShow = true;
			});
		  }
		 
	    $scope.show = function(){
	      $scope.showAddEditDish = true;
	      $scope.searchShow = false;
	      $scope.findNm=null;
	    };

	    $scope.hide = function(){
	      $scope.showAddEditDish = false;
	      $scope.dish = {};
	    };

	    $scope.editDish = function(dish){
	    	$scope.dish = angular.copy(dish);
	    	
		      var newElem = { "name": dish.restaurant.name };
		      
		      $scope.restaurants= [];
		      $scope.restaurants.push(newElem);		    
		      $scope.selectedDevice = newElem.name;
	      
	      $scope.show();
	    };

	    $scope.saveDish = function(dish) {
	      if (dish.id) {
	        RestSrv.edit(urlDish, dish, function() {

	          for (var i = 0; i < $scope.plates.length; i++) {
	            if ($scope.plates[i].id === dish.id) {
	              $scope.plates[i] = dish;
	            }
	          }
	          $scope.hide();
	        });
	      } else {
	        RestSrv.add(urlDish, dish, function(newDish) {
	          $scope.plates.push(newDish);
	          $scope.hide();
	        });
	      }
	    };
	    
	      $scope.deleteDish = function(dish) {
			   RestSrv.delete(urlDish, dish, function() {
				   $scope.plates.splice($scope.plates.indexOf(dish), 1);
				});
			};
  });