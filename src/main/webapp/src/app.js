'use strict';

var BASE_URL = 'http://localhost:8081/api';

angular.module('restaurant', ["ngCookies",'ngRoute','ngStorage', 'ngMaterial', 'ngMessages', 'ngMaterial'])
	.constant('SERVICE_PATH', {
	    'ROOT_PATH': BASE_URL,
	    'PUBLIC_PATH': BASE_URL + '/public',
	    'PRIVATE_PATH': BASE_URL + '/private'
	  })
    .config(function($routeProvider) {
	  $routeProvider
	  .when("/", {
	    templateUrl : "src/login/login.html",
	    controller : "loginCtrl"
	  })
	  .when('/home', {
	     templateUrl: 'src/home/home.html',
	     controller: 'homeCtrl'
	    })
	  .when("/restaurant", {
	    templateUrl : "src/restaurant/restaurant.html",
	    controller : "restaurantCtrl"
	  })
	  .when('/dish', {
	     templateUrl: 'src/dish/dish.html',
	     controller: 'dishCtrl'
	    })
	  .when('/user', {
	     templateUrl: 'src/user/user.html',
	     controller: 'userCtrl'
	    })
	  .otherwise({
	      redirectTo: '/'
	    }); 
	})
  .config(function($httpProvider) {
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    $httpProvider.defaults.withCredentials = true;
    $httpProvider.interceptors.push('httpRequestInterceptor');
  })
  .run(function($rootScope, LoginLogoutSrv) {
    $rootScope.authDetails = { name: '', authenticated: false, permissions: [] };
    LoginLogoutSrv.verifyAuth();     
  })
  .run(function($rootScope) {
	    $rootScope.statusMenu = false;
  });

  
