package com.cedrotech.utils;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.StandardPasswordEncoder;

import com.cedrotech.RestauranteApplication;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackageClasses = RestauranteApplication.class)
public class AppConfig {

    @Bean(name = "applicationProperty")
    public ApplicationProperty getApplicationProperty() {
        return new ApplicationProperty();
    }

    @Bean(name = "passwordEncoder")
    public StandardPasswordEncoder getStandardPasswordEncoder() {
        return new StandardPasswordEncoder(getApplicationProperty().getSecret());
    }

}
