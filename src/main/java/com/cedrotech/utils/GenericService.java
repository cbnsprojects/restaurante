package com.cedrotech.utils;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public abstract class GenericService<T, ID extends Serializable> implements ServiceMap {

	private final Logger LOGGER = Logger.getLogger(this.getClass());

	@Autowired
	protected JpaRepository<T, ID> genericRepository;
	
	@RequestMapping(method = RequestMethod.GET)
	public List<T> findAll() {
		if (this.LOGGER.isDebugEnabled()) {
			this.LOGGER.debug("Todos Registros.");
		}
		return this.genericRepository.findAll();
	}

	@Transactional
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> insert(@RequestBody @Validated T entity, Errors erros) {
		if (this.LOGGER.isDebugEnabled()) {
			this.LOGGER.debug(String.format("Salvar Registro [%s].", entity));
		}

		this.genericRepository.save(entity);
		
		return ResponseEntity.status(HttpStatus.OK).body("criado com sucesso");
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<?> update(@RequestBody @Validated T entity, Errors errors) {
		this.LOGGER.debug(String.format("Atualizar Registro [%s].", entity));

		//this.genericRepository.save(entity);
		this.genericRepository.saveAndFlush(entity);
		
		return ResponseEntity.status(HttpStatus.OK).body("Atualizado com sucesso");		
	}

	@RequestMapping(method = RequestMethod.DELETE)
	public void delete(@RequestBody T entity) {
		this.LOGGER.debug(String.format("Excluir Registro [%s].", entity));

		this.genericRepository.delete(entity);
	}

}
