package com.cedrotech.security;

import java.security.Principal;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cedrotech.utils.ResourcePaths;
import com.cedrotech.utils.ServiceMap;

@RestController
@RequestMapping(ResourcePaths.LOGIN_PATH)
public class SecurityService implements ServiceMap {

    @RequestMapping(method = { RequestMethod.GET })
    public Principal user(Principal user) {
        return user;
    }

}
