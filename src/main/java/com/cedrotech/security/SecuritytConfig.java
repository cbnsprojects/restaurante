package com.cedrotech.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.cedrotech.utils.ResourcePaths;

@EnableWebSecurity
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class SecuritytConfig extends WebSecurityConfigurerAdapter{

    public static final String AUTH_USER = "ROLE_USER";

    public static final String AUTH_ADMIN = "ROLE_ADMIN";
    
    @Autowired
    private UserDetailsService userService;
//
    @Autowired
    private PasswordEncoder passwordEncoder;
	
    @Autowired
    private HeaderHandler headerHandler;
    
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication()
//        .withUser("caio").password("123").roles("ADMIN")
//        .and()
//        .withUser("admin").password("admin").roles("ADMIN");
    	auth.userDetailsService(this.userService).passwordEncoder(this.passwordEncoder);
    }
    
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		//http.authorizeRequests().anyRequest().authenticated()
		
		http.httpBasic().and().authorizeRequests()
			.antMatchers("/css/**", "/assets/**", "/src/**").permitAll()
			.antMatchers("/").permitAll()
			.antMatchers("index.html").permitAll()
			.antMatchers(ResourcePaths.PUBLIC_ROOT_PATH + ResourcePaths.ALL).permitAll()
			.antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
			 //Authorities
			//.antMatchers(HttpMethod.GET, ResourcePaths.HOME_PATH).permitAll()
			.antMatchers(HttpMethod.GET, ResourcePaths.USER_PATH).hasAnyAuthority(AUTH_ADMIN)
			.antMatchers(HttpMethod.POST, ResourcePaths.USER_PATH).hasAnyAuthority(AUTH_ADMIN)
			.antMatchers(HttpMethod.PUT, ResourcePaths.USER_PATH).hasAnyAuthority(AUTH_ADMIN)
			.antMatchers(HttpMethod.DELETE, ResourcePaths.USER_PATH).hasAnyAuthority(AUTH_ADMIN)
			
			.antMatchers(HttpMethod.POST, ResourcePaths.RESTAURANT_PATH).hasAnyAuthority(AUTH_ADMIN)
			.antMatchers(HttpMethod.PUT, ResourcePaths.RESTAURANT_PATH).hasAnyAuthority(AUTH_ADMIN)
			.antMatchers(HttpMethod.DELETE, ResourcePaths.RESTAURANT_PATH).hasAnyAuthority(AUTH_ADMIN)
			
			.anyRequest().fullyAuthenticated().and()
			.logout().logoutRequestMatcher(new AntPathRequestMatcher(ResourcePaths.LOGOUT_PATH)).logoutSuccessHandler(headerHandler)
            .and()
            
			//.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			//.and()
            .csrf().disable()
            .addFilterAfter(headerHandler, ChannelProcessingFilter.class);
		;
	}
	

//	@Bean
//	public PasswordEncoder passwordEncoder() {
//		return (PasswordEncoder) NoOpPasswordEncoder.getInstance();
//	}
	
}
