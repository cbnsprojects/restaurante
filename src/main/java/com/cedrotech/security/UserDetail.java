package com.cedrotech.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.cedrotech.entity.PermissionEntity;
import com.cedrotech.entity.UserEntity;
import com.cedrotech.repository.UserRepository;

@Component
public class UserDetail implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        UserEntity user = this.userRepository.findByName(name);

        if (user == null) {
            throw new UsernameNotFoundException("Usuario \"" + name + "\" nao encontrado");
        }

        LoginDetailBean login = new LoginDetailBean(user.getName(), user.getPassword());

        for (PermissionEntity permission : user.getPermissions()) {
            login.addRole(permission.getRole());
        }

        return login;
    }

}