package com.cedrotech.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cedrotech.entity.RestaurantEntity;
import com.cedrotech.repository.RestaurantRepository;

@Service
public class RestaurantService {

	@Autowired
	private RestaurantRepository restaurantRepository;
	
//	@Autowired
//	private DishRepository dishRepository;

	public List<RestaurantEntity> findAll() throws Exception {
		return restaurantRepository.findAll();
	}
	
	public RestaurantEntity findByName(String name) throws Exception {
		return restaurantRepository.findByName(name);
	}
	
	public RestaurantEntity findById(Long id) {
		return restaurantRepository.findOne(id);
	}
		
	public RestaurantEntity save(RestaurantEntity restaurant) {
		return restaurantRepository.save(restaurant);
	}

	public RestaurantEntity update(RestaurantEntity restaurant) throws Exception {
		restaurantRepository.findOne(restaurant.getId());
		return restaurantRepository.save(restaurant);
	}
	
	public RestaurantEntity delete(RestaurantEntity restaurant) throws Exception {
//		RestaurantEntity restaurant = restaurantRepository.findOne(entity.getId());
//		List<DishEntity> dish = restaurant.getDishEntity();
//		
//		for(DishEntity prato: dish) {
//			dishRepository.delete(prato);
//		}
		
		restaurantRepository.delete(restaurant);
		return restaurant;  
	}

}
