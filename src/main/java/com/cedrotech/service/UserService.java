package com.cedrotech.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cedrotech.entity.UserEntity;
import com.cedrotech.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	public UserEntity findByName(String name) throws Exception {
		return userRepository.findByName(name);
	}
}
