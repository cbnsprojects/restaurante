package com.cedrotech.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cedrotech.entity.DishEntity;
import com.cedrotech.repository.DishRepository;

@Service
public class DishService {

	@Autowired
	private DishRepository dishRepository;
	

	public DishEntity findByName(String name) throws Exception {
		return dishRepository.findByName(name);
	}
	
	public List<DishEntity> findAll() {
		return (List<DishEntity>) dishRepository.findAll();
	}
	
	public DishEntity save(DishEntity dish) {
		return dishRepository.save(dish);
	}
	
	public DishEntity update(DishEntity dish) throws Exception {
		dishRepository.findOne(dish.getId());
		return dishRepository.save(dish);
	}
	
//	public DishEntity delete(DishEntity dish) throws Exception {
//		
//		this.dishRepository.delete(dish);
//		return null;
//	}
	
	public DishEntity delete(DishEntity dish) throws Exception {
		//List<DishEntity> dish1 = restaurant.getDishEntity();
		dishRepository.delete(dish);
		return dish;
	}

}
