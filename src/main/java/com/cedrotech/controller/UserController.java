package com.cedrotech.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cedrotech.entity.UserEntity;
import com.cedrotech.service.UserService;
import com.cedrotech.utils.GenericService;
import com.cedrotech.utils.ResourcePaths;

import javassist.NotFoundException;

@RestController
@RequestMapping(path = ResourcePaths.USER_PATH)
public class UserController extends GenericService<UserEntity, Long> {

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private UserService userService;
	
	@GetMapping(value="/{name}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<UserEntity> findByName(@PathVariable String name) throws Exception {
		UserEntity usuario = userService.findByName(name);
        if (usuario == null) {
        	throw new NotFoundException("Usuario não encontrado");
        }
		return ResponseEntity.ok().body(usuario);
	}
	
	@Override
	public ResponseEntity<?> insert(@RequestBody UserEntity user, Errors erros) {
		user.setPassword(this.passwordEncoder.encode(user.getPassword()));
		return super.insert(user, erros);
	}

	public ResponseEntity<?> update(@RequestBody UserEntity user, Errors errors) {
		user.setPassword(this.passwordEncoder.encode(user.getPassword()));
		return super.update(user, errors);
	}

}
