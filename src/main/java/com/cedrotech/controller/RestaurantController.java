package com.cedrotech.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cedrotech.entity.RestaurantEntity;
import com.cedrotech.service.RestaurantService;
import com.cedrotech.utils.ResourcePaths;
import com.cedrotech.utils.ServiceMap;

import javassist.NotFoundException;

@RestController
@RequestMapping(path = ResourcePaths.RESTAURANT_PATH)
public class RestaurantController implements ServiceMap {

	@Autowired
	private RestaurantService restaurantService;
	
	
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<RestaurantEntity>> findAll() throws Exception {
		
		return ResponseEntity.ok().body(restaurantService.findAll());
	}
	
	@GetMapping(value="/id/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<RestaurantEntity> findById(@PathVariable Long id) throws Exception {
		RestaurantEntity restaurant = restaurantService.findById(id);
		return ResponseEntity.ok().body(restaurant);
	}
	
	@GetMapping(value="/{name}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<RestaurantEntity> findByName(@PathVariable String name) throws Exception {
		if(name == null)
			throw new NotFoundException("Restaurante não informado! Impossivel localizar!");
		
		RestaurantEntity restaurant = restaurantService.findByName(name);
		
        if (restaurant == null) throw new NotFoundException("Restaurante não encontrado! ");
        
		return ResponseEntity.ok().body(restaurant);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<RestaurantEntity> save(@Valid @RequestBody RestaurantEntity restaurant) throws Exception {
		RestaurantEntity newRestaurant = restaurantService.save(restaurant);
		
		return ResponseEntity.ok().body(newRestaurant);
	}
	
	@PutMapping
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<RestaurantEntity> update(@Valid @RequestBody RestaurantEntity restaurant) throws Exception {
		RestaurantEntity objUpdated = restaurantService.update(restaurant);
		return ResponseEntity.ok().body(objUpdated);
	}
	
	@DeleteMapping
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<RestaurantEntity> delete(@Valid @RequestBody RestaurantEntity restaurant) throws Exception {
		return ResponseEntity.ok().body(restaurantService.delete(restaurant));
	}
}
