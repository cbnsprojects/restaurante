package com.cedrotech.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cedrotech.entity.DishEntity;
import com.cedrotech.service.DishService;
import com.cedrotech.utils.ResourcePaths;
import com.cedrotech.utils.ServiceMap;

import javassist.NotFoundException;

@RestController
@RequestMapping(path = ResourcePaths.DISH_PATH)
public class DishController implements ServiceMap {

	@Autowired
	private DishService dishService;
	
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<DishEntity>> findAll() throws Exception {
		
		return ResponseEntity.ok().body(dishService.findAll());
	}
	
	@GetMapping(value="/{name}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<DishEntity> findByName(@PathVariable String name) throws Exception {
		DishEntity dish = dishService.findByName(name);
        if (dish == null) {
            throw new NotFoundException("Prato não encontrado");
        }
		return ResponseEntity.ok().body(dish);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<DishEntity> save(@Valid @RequestBody DishEntity dish) throws Exception {
		DishEntity newDish = dishService.save(dish);
		return ResponseEntity.ok().body(newDish);
	}
	
	@PutMapping
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<DishEntity> update(@Valid @RequestBody DishEntity dish) throws Exception {
		DishEntity objUpdated = dishService.update(dish);
		return ResponseEntity.ok().body(objUpdated);
	}
	
	@DeleteMapping
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<DishEntity> delete(@Valid @RequestBody DishEntity dish) throws Exception {
		return ResponseEntity.ok().body(dishService.delete(dish));
	}
	
}
