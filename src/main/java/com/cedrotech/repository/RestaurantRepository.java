package com.cedrotech.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.cedrotech.entity.RestaurantEntity;

public interface RestaurantRepository extends JpaRepository<RestaurantEntity, Long> {

	public RestaurantEntity findByName(@Param("name") String name);
	
	List<RestaurantEntity> findAllByOrderByNameAsc();
	
	public List<RestaurantEntity> findAllByName(@Param("name") String name);

}
