package com.cedrotech.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.cedrotech.entity.DishEntity;

public interface DishRepository extends JpaRepository<DishEntity, Long> {

	public DishEntity findByName(@Param("name") String name);
	
	public List<DishEntity> findAll();
	
	public List<DishEntity> findAllByName(@Param("name") String name);
	
}
