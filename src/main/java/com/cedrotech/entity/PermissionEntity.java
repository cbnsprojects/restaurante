package com.cedrotech.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "tb_permission")
public class PermissionEntity implements Serializable {

    private static final long serialVersionUID = 201602010401L;

	@Id
	@Column(name = "id_permission", nullable = false , length = 20)
	private Long id;
	
    @NotNull
    @NotEmpty
    @Column(name = "role", length = 45, nullable = false, unique = true)
    private String role;

    public PermissionEntity() {
    }

    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}