package com.cedrotech.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="tb_dish")
@SequenceGenerator(name = "restaurante.tb_dish_id_dish_seq", sequenceName = "restaurante.tb_dish_id_dish_seq", allocationSize = 1, initialValue = 1)
public class DishEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO,  generator = "restaurante.tb_dish_id_dish_seq")
	@Column(name = "id_dish", nullable = false , length = 20)
	private Long id;
	
	@NotNull
	@Size(min = 3, max = 80)
	@Column(name = "name", nullable = false , length = 7)
	private String name;
	
	@NotNull
	@Column(name = "price", nullable = false , length = 7)
	private BigDecimal price;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "id_restaurant", nullable = false)
	@JsonIgnoreProperties("dishEntity")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private RestaurantEntity restaurant;

//	@NotNull
//	@Column(name = "id_restaurant", nullable = false , length = 7)
//	private Long idRestaurant;

	public DishEntity() {
	
	}
	
	public DishEntity(Long id, String name, BigDecimal price, RestaurantEntity restaurant) {
		this.id = id;
		this.name = name;
		this.price = price;
		this.restaurant = restaurant;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	public RestaurantEntity getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(RestaurantEntity restaurant) {
		this.restaurant = restaurant;
	}

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
