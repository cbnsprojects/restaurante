package com.cedrotech.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cascade;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "tb_restaurant")
@SequenceGenerator(name = "restaurante.tb_restaurant_id_restaurant_seq", sequenceName = "restaurante.tb_restaurant_id_restaurant_seq", allocationSize = 1, initialValue = 1)
public class RestaurantEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO,  generator = "restaurante.tb_restaurant_id_restaurant_seq")
	@Column(name = "id_restaurant", nullable = false , length = 20)
	private Long id;
	
	@NotNull
	@Size(min = 3, max = 80)
	@Column(name = "name", nullable = false , length = 7)
	private String name;
	
	@NotNull
	@Size(min = 3, max = 80)
	@Column(name = "telephone", nullable = false , length = 7)
	private String telephone;

	@NotNull
	@Size(min = 3, max = 80)
	@Column(name = "address", nullable = false , length = 10)
	private String address;
	
	@OrderBy("name ASC")
	@OneToMany(mappedBy = "restaurant", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
	@Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
	@JsonIgnoreProperties("restaurant")
	private List<DishEntity> dishEntity;
	
//	@OrderBy("name ASC")
//	@OneToMany()
//	@JoinColumn(name = "id_restaurant")
	//, orphanRemoval=true
//    private List<DishEntity> dishEntity;
	
	
//	public List<DishEntity> getDishEntity() {
//		return dishEntity;
//	}
//
//	public void setDishEntity(List<DishEntity> dishEntity) {
//		this.dishEntity = dishEntity;
//	}
	
	public RestaurantEntity() {
    }
	
	public RestaurantEntity(String name, String telephone, String address) {
		super();
		this.name = name;
		this.telephone = telephone;
		this.address = address;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
    public List<DishEntity> getDishEntity() {
		return dishEntity;
	}

	public void setDishEntity(List<DishEntity> dishEntity) {
		this.dishEntity = dishEntity;
	}

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
