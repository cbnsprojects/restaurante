package com.cedrotech.test.utils;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.cedrotech.utils.AppConfig;

@Configuration
@Import(value = { AppConfig.class })
@ComponentScan(basePackages = {"com.cedrotech.test"})
public class AppConfigTest {

}

