package com.cedrotech.test.repository;

import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.cedrotech.entity.DishEntity;
import com.cedrotech.entity.RestaurantEntity;
import com.cedrotech.repository.DishRepository;
import com.cedrotech.repository.RestaurantRepository;
import com.cedrotech.test.utils.AbstractTest;

public class DishRepositoryTest extends AbstractTest {

	private static final Logger LOGGER = Logger.getLogger(UserRepositoryTest.class);

	@Autowired
	private DishRepository dishRepository;
	
    @Autowired
    private RestaurantRepository restaurantRepository;

    @Test
    public void findByNameTest() {
    	DishEntity dish = this.dishRepository.findByName("Admin");

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Test FindByName(): " + dish);
        }
    }
    
    @Test
    public void addTest() {
        RestaurantEntity restaurante = new RestaurantEntity();
        restaurante.setName("Restaurante Teste prato");
        restaurante.setTelephone("33333");
        restaurante.setAddress("Rua professor");
        
        restaurantRepository.save(restaurante);
        
        DishEntity prato = new DishEntity();
        prato.setName("arroz ao alho");
        prato.setPrice(BigDecimal.TEN);
        prato.setRestaurant(restaurante);

        prato = this.dishRepository.save(prato);

        LOGGER.info("Test Add prato: "+ prato);
    }
    
    @Test
    public void updateTest() {
    	DishEntity pratoFind = this.dishRepository.findByName("Prato Teste");
    	
    	if (pratoFind != null) {
    		pratoFind.setName("Prato Teste Update");
            LOGGER.info("Test update usuario" + this.dishRepository.save(pratoFind));   
            assertNotNull(pratoFind);
    	} 
    }
    
    @Test
    public void deleteTest() {
    	DishEntity pratoFind = this.dishRepository.findByName("Prato Teste");
    	
    	if (pratoFind != null) {
    		this.dishRepository.delete(pratoFind);
    	} 
        LOGGER.info("Test delete usuario");
    }

}
