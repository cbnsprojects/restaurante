package com.cedrotech.test.repository;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.cedrotech.entity.UserEntity;
import com.cedrotech.repository.UserRepository;
import com.cedrotech.test.utils.AbstractTest;

public class UserRepositoryTest extends AbstractTest {

    private static final Logger LOGGER = Logger.getLogger(UserRepositoryTest.class);

    @Autowired
    private UserRepository userRepository;

    @Test
    public void findAllTest() {
        List<UserEntity> users = this.userRepository.findAll();

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Test FindAll(): " + users);
        }
    }

    @Test
    public void findByNameTest() {
    	UserEntity user = this.userRepository.findByName("Admin");

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Test FindByName(): " + user);
        }
    }
    
    @Test
    public void addTest() {
        String name = "Usuario Teste";
        String password = "1234";

        UserEntity usuario = new UserEntity();
        usuario.setName(name);
        usuario.setPassword(password);

        usuario = this.userRepository.save(usuario);

        LOGGER.info("Test Add usuario: "+ usuario);
    }

    @Test
    public void updateTest() {
      	UserEntity usuarioFind = this.userRepository.findByName("Usuario Teste");
    	
    	if (usuarioFind != null) {
    			usuarioFind.setName("Usuario Teste Update");
                LOGGER.info("Test update usuario" + this.userRepository.save(usuarioFind));        		
    		/* Test Update */
            assertNotNull(usuarioFind);
    	} 
    }
    
    @Test
    public void deleteTest() {
    	UserEntity usuarioFind = this.userRepository.findByName("Usuario Teste Update");
    	
    	if (usuarioFind != null) {
        		LOGGER.info("Test delete usuario" + usuarioFind);
        		this.userRepository.delete(usuarioFind);        		        		
    	} 
    }

}
