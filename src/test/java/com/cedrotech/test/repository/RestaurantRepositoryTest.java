package com.cedrotech.test.repository;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.cedrotech.entity.RestaurantEntity;
import com.cedrotech.repository.RestaurantRepository;
import com.cedrotech.test.utils.AbstractTest;

public class RestaurantRepositoryTest extends AbstractTest {

	private static final Logger LOGGER = Logger.getLogger(UserRepositoryTest.class);
	
    @Autowired
    private RestaurantRepository restaurantRepository;
    
    @Test
    public void findAllTest() {
        List<RestaurantEntity> restaurantes = this.restaurantRepository.findAllByOrderByNameAsc();

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Test FindAll(): " + restaurantes);
        }
    }
    
    @Test
    public void findByNameTest() {
    	RestaurantEntity restaurante = this.restaurantRepository.findByName("alibaba");

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Test FindByName(): " + restaurante);
        }
    }
    
    @Test
    public void addTest() {
        String name = "Restaurante Teste";
        String telephone = "99996666";
        String address = "Rua alipo abraao";

        RestaurantEntity restaurante = new RestaurantEntity();
        restaurante.setName(name);
        restaurante.setTelephone(telephone);
        restaurante.setAddress(address);
        
        restaurante = this.restaurantRepository.save(restaurante);

        LOGGER.info("Test Add usuario: " + restaurante);
    }
    
    @Test
    public void updateTest() {
    	List<RestaurantEntity> restauranteFind = this.restaurantRepository.findAllByName("Restaurante Teste");
    	
    	if (restauranteFind != null) {
        	for(RestaurantEntity restaurantUpdate: restauranteFind) {
        		restaurantUpdate.setName("Restaurante Teste Update");
                this.restaurantRepository.save(restauranteFind);        		
        	}
    	} 

    }
    
    @Test
    public void deleteTest() {
    	List<RestaurantEntity> restauranteFind = this.restaurantRepository.findAllByName("Restaurante Teste");
    	
    	if (restauranteFind != null) {
        	for(RestaurantEntity restaurantDelete: restauranteFind) {
        		this.restaurantRepository.delete(restaurantDelete);        		        		
        	}
    	} 
    	
        LOGGER.info("Test delete usuario" );
    }
}
